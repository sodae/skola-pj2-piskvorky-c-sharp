﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Core;
using Core.History;
using Core.Players;

namespace Tictactoe
{
    public partial class Window : Form
    {
        delegate void AddLogCallback(string text);

        private delegate void SetBoxContentCallback(int x, int y, String text);

        public Window()
        {
            InitializeComponent();
        }

        public Box[,] Boxs;

        public void SetBoxText(int x, int y, String text)
        {
            if (this.Boxs[x, y].InvokeRequired)
            {
                SetBoxContentCallback d = new SetBoxContentCallback(this.SetBoxText);
                this.Invoke(d, new object[] { x, y, text });
            }
            else
            {
                this.Boxs[x, y].Text = text;
            }
        }

        public void addLog(String text)
        {
            if (this.log.InvokeRequired)
            {
                AddLogCallback d = new AddLogCallback(this.addLog);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.log.AppendText(text + "\n");
            }
        }

        public void CreateField(int countX, int countY)
        {
            Boxs = new Box[countX, countY];
            int buttonWidth = this.buttonsContainer.Width / countX;
            int buttonHeight = this.buttonsContainer.Height / countY;
            int distance = 0;

            for (int x = 0; x < countX; x++)
            {
                for (int y = 0; y < countY; y++)
                {
                    Box box = new Box(x, y)
                    {
                        Top = (x*buttonHeight + distance),
                        Left = (y*buttonWidth + distance),
                        Width = buttonWidth,
                        Height = buttonHeight
                    };
                    // Possible add Buttonclick event etc..
                    box.Click += this.receiveClick;
                    this.buttonsContainer.Controls.Add(box);
                    this.Boxs[x, y] = box;
                }

            }
        }

        private void Window_Load(object sender, EventArgs e)
        {

        }

        private Position lastClick;

        readonly object lockingObject = new object();

        public Position waitOnClick()
        {
            lock (this.lockingObject)
            {
                Monitor.Wait(this.lockingObject);
                Position click = this.lastClick;
                this.lastClick = null;
                return click;

            }
        }

        private void receiveClick(Object sender, EventArgs e)
        {
            lock (this.lockingObject)
            {
                Box box = (Box)sender;
                this.lastClick = new Position(box.X, box.Y);
                Monitor.Pulse(this.lockingObject);
            }
        }

        private Thread lastGameThread;

        private GameHistory gameHistory;

        private void startGame(int width, int height, GameHistory gameHistory = null)
        {
            this.buttonsContainer.Controls.Clear();
            this.Boxs = null;
            this.CreateField(width, height);

            if (lastGameThread != null) lastGameThread.Abort();
            this.gameHistory = gameHistory ?? new GameHistory();

            lastGameThread = new Thread(new ThreadStart(() =>
            {
                Game game = new Game(new Board(width, height));
                game.AddListener(new GameListener(this));
                game.AddListener(new Listener(this.gameHistory));
                game.SetPlayerO(new Player(this, "Martin"));
                game.SetPlayerX(new Player(this, "Lucka"));
                if (gameHistory == null)
                {
                    game.Start();
                }
                else
                {
                    game.ContinueHistory(gameHistory);
                }
            }));
            lastGameThread.Start();
        }

        private void start_Click(object sender, EventArgs e)
        {
            this.startGame(20, 20);
        }

        private void save_button_Click(object sender, EventArgs e)
        {
            this.gameHistory.saveToFile("file.bin");
        }

        private void loadsave_button_Click(object sender, EventArgs e)
        {
            GameHistory gameHistory = GameHistory.createFromFile("file.bin");
            this.startGame(20, 20, gameHistory);
        }

    }
}
