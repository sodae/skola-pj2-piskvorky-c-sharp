﻿namespace Tictactoe
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonsContainer = new System.Windows.Forms.Panel();
            this.start = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.RichTextBox();
            this.loadsave_button = new System.Windows.Forms.Button();
            this.save_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonsContainer
            // 
            this.buttonsContainer.Location = new System.Drawing.Point(12, 12);
            this.buttonsContainer.Name = "buttonsContainer";
            this.buttonsContainer.Size = new System.Drawing.Size(400, 400);
            this.buttonsContainer.TabIndex = 0;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(419, 13);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 1;
            this.start.Text = "start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // log
            // 
            this.log.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.log.Location = new System.Drawing.Point(419, 43);
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(239, 369);
            this.log.TabIndex = 2;
            this.log.Text = "";
            // 
            // loadsave_button
            // 
            this.loadsave_button.Location = new System.Drawing.Point(501, 13);
            this.loadsave_button.Name = "loadsave_button";
            this.loadsave_button.Size = new System.Drawing.Size(75, 23);
            this.loadsave_button.TabIndex = 3;
            this.loadsave_button.Text = "Load";
            this.loadsave_button.UseVisualStyleBackColor = true;
            this.loadsave_button.Click += new System.EventHandler(this.loadsave_button_Click);
            // 
            // save_button
            // 
            this.save_button.Location = new System.Drawing.Point(583, 13);
            this.save_button.Name = "save_button";
            this.save_button.Size = new System.Drawing.Size(75, 23);
            this.save_button.TabIndex = 4;
            this.save_button.Text = "Save";
            this.save_button.UseVisualStyleBackColor = true;
            this.save_button.Click += new System.EventHandler(this.save_button_Click);
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 426);
            this.Controls.Add(this.save_button);
            this.Controls.Add(this.loadsave_button);
            this.Controls.Add(this.log);
            this.Controls.Add(this.start);
            this.Controls.Add(this.buttonsContainer);
            this.Name = "Window";
            this.Text = "Piškvorky";
            this.Load += new System.EventHandler(this.Window_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel buttonsContainer;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.RichTextBox log;
        private System.Windows.Forms.Button loadsave_button;
        private System.Windows.Forms.Button save_button;

    }
}

