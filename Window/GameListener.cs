﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Tictactoe
{
    class GameListener:IGameListener
    {
        private Tictactoe.Window window;

        public GameListener(Window window)
        {
            this.window = window;
        }

        public void OnGameStarted(Game game)
        {
            this.window.addLog("Game started");
        }

        public void OnGameFinished(Game game, IPlayer winner)
        {
            this.window.addLog("Game finnished " + winner.ToString());
        }

        public void OnInvalidMove(Game game, IPlayer player, Position place)
        {
            this.window.addLog(String.Format("Invalid move {0} na X {1} a Y {2}", player.ToString(), place.X, place.Y));
        }

        public void OnMove(Game game, IPlayer player, Position place)
        {
            this.window.addLog(String.Format("Move {0} na X {1} a Y {2}", player.ToString(), place.X, place.Y));

            BoardView boardView = game.BoardView;

            for (int x = 0; x < boardView.Width; x++)
            {
                for (int y = 0; y < boardView.Height; y++)
                {
                    Board.Player onBoardPlayer = boardView[x, y];
                    var o = "";
                    if (onBoardPlayer == Board.Player.None) o = " ";
                    if (onBoardPlayer == Board.Player.X) o = "X";
                    if (onBoardPlayer == Board.Player.O) o = "O";
                    window.SetBoxText(x, y, o);
                }
            }
        }
    }
}
