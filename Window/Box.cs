﻿using System.Windows.Forms;

namespace Tictactoe
{
    public class Box: Button
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Box(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}