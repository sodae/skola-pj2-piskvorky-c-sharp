﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core;

namespace Tictactoe
{
    class Player : IPlayer
    {
        private String name;

        private Window window;

        public Player(Window window, String name)
        {
            this.name = name;
            this.window = window;
        }

        public override string ToString()
        {
            return name;
        }

        public Position Move(BoardView boardView, Position lastMove)
        {
            return window.waitOnClick();
        }
    }
}
