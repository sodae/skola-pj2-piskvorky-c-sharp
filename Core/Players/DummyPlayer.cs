﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Players
{
    public class DummyPlayer: IPlayer
    {
        private String name;

        private Random randomer;

        public DummyPlayer(String name)
        {
            this.name = name;
            this.randomer = new Random();
        }

        public Position Move(BoardView board, Position lastMove)
        {
            return new Position(randomer.Next(0, board.Width), randomer.Next(0, board.Height));
        }

        public override string ToString()
        {
            return name;
        }
    }
}
