﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.History;

namespace Core
{
    public class Game
    {
        private readonly Board board;

        public readonly BoardView BoardView;

        public IPlayer PlayerO { get; private set; }

        public IPlayer PlayerX { get; private set; }

        private readonly IList<IGameListener> listeners;

        public int WinningLength { set; get; }

        public Game(Board board)
        {
            this.board = board;
            this.BoardView = new BoardView(board);
            this.listeners = new List<IGameListener>();
            this.WinningLength = 5;
        }

        public void AddListener(IGameListener listener)
        {
            this.listeners.Add(listener);
        }

        public void SetPlayerO(IPlayer player)
        {
            this.PlayerO = player;
        }

        public void SetPlayerX(IPlayer player)
        {
            this.PlayerX = player;
        }

        public IPlayer ContinueHistory(GameHistory history)
        {
            if (this.PlayerO == null || this.PlayerX == null)
                throw new PlayerIsNotSetException();

            if (history == null) throw new NullReferenceException();

            Board.Player lastBoardPlayer = Board.Player.None;
            Position lastPosition = null;

            foreach (Move move in history.GetEnumerable())
            {
                board.SetMove(move.Player, move.Position);
                lastBoardPlayer = move.Player;
                lastPosition = move.Position;
            }

            IPlayer lastPlayed = lastBoardPlayer == Board.Player.O ? this.PlayerO : this.PlayerX;

            foreach (var listener in listeners)
                listener.OnMove(this, lastPlayed, lastPosition);

            if (this.BoardView.CheckIsWon(lastBoardPlayer, this.WinningLength, lastPosition))
            {
                return lastPlayed;
            }

            return _play(lastPlayed, lastPosition);
        }

        public IPlayer Start(IPlayer starting = null)
        {
            if (this.PlayerO == null || this.PlayerX == null)
                throw new PlayerIsNotSetException();

            foreach (var listener in listeners)
                listener.OnGameStarted(this);

            IPlayer lastPlayed = starting == this.PlayerO ? this.PlayerX : this.PlayerO; // before first move is switched
            return _play(lastPlayed, null);
        }

        private IPlayer _play(IPlayer lastPlayed, Position lastPosition)
        {
            IPlayer playing = null;
            for (; ; )
            {
                try
                {
                    playing = this.PlayerO == lastPlayed ? this.PlayerX : this.PlayerO;
                    Position position = playing.Move(this.BoardView, lastPosition);

                    Board.Player boardPlayer = this.PlayerO == playing ? Board.Player.O : Board.Player.X;
                    this.board.SetMove(boardPlayer, position);

                    foreach (var listener in listeners)
                        listener.OnMove(this, playing, position);

                    if (this.BoardView.CheckIsWon(boardPlayer, this.WinningLength, position))
                    {
                        foreach (var listener in listeners)
                            listener.OnGameFinished(this, playing);
                        return playing;
                    }

                    lastPosition = position;
                    lastPlayed = playing;
                }
                catch (InvalidMoveException e)
                {
                    foreach (var listener in listeners)
                        listener.OnInvalidMove(this, playing, e.Position);
                }
            }
        }

    }

    public class PlayerIsNotSetException : Exception
    {
    }
}
