﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public interface IPlayer
    {
        Position Move(BoardView boardView, Position lastMove);
    }
}

