﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public interface IGameListener
    {
        void OnGameStarted(Game game);

        void OnGameFinished(Game game, IPlayer winner);

        void OnInvalidMove(Game game, IPlayer player, Position place);

        void OnMove(Game game, IPlayer player, Position place);
    }
}
