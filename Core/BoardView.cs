﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Core
{
    public class BoardView
    {
        private Board board;

        public enum Direction
        {
            InWay = 1,
            Stay = 0,
            Backwards = -1
        }

        public BoardView(Board board)
        {
            this.board = board;
        }

        public int Width
        {
            get { return board.Width; }
        }

        public int Height
        {
            get { return board.Height; }
        }

        public Board.Player this[int x, int y]
        {
            get { return this.GetPosition(x, y); }
        }

        public Board.Player GetPosition(int x, int y)
        {
            return this.board.GetPosition(x, y);
        }

        public bool CheckIsWon(Board.Player player, int winningLength, Position startPosition)
        {
            foreach (BoardView.Direction horizontal in Enum.GetValues(typeof(BoardView.Direction)))
            {
                foreach (BoardView.Direction vertical in Enum.GetValues(typeof(BoardView.Direction)))
                {
                    if (this.FindRow(player, startPosition.X, startPosition.Y, horizontal, vertical, winningLength))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool FindRow(Board.Player want, int x, int y, Direction right, Direction down, int remainingLength)
        {
            if (right == Direction.Stay && down == Direction.Stay) return false;
            if (remainingLength == 0) return true;
            if (x < 0 || y < 0 || x >= this.board.Width || y >= this.board.Height) return false;
            if (this.board.GetPosition(x, y) != want) return false;
            return FindRow(want, x + (int)right, y + (int)down, right, down, remainingLength-1);
        }

    }
}
