﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Core
{

    /**
     * Example of config
     * 
     <?xml version="1.0" encoding="utf-8" standalone="yes"?>
        <root>
        <board>
          <width>20</width>
          <height>20</height>
        </board>
      </root>
     */
    public class XmlBoardCreator
    {
        public delegate Board DefaultBoard();

        public static Board CreateBoard(String filename, DefaultBoard defaultBoard)
        {
            try
            {
                var xml = XDocument.Load(filename);
                var query = (from c in xml.Root.Descendants("board")
                    select new
                    {
                        width = c.Element("width").Value,
                        height = c.Element("height").Value
                    }).Take(1);

                int width = Int32.Parse(query.First().width);
                int height = Int32.Parse(query.First().height);
                return new Board(width, height);
            }
            catch (NullReferenceException e)
            {
                return defaultBoard();
            }
            catch (FileNotFoundException e)
            {
                return defaultBoard();
            }

        }
    }
}
