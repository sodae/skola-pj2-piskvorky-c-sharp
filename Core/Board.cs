﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
    public class Board
    {

        public enum Player
        {
            None = 0,
            X = 1,
            O = 2
        }

        public int Width { private set; get; }

        public int Height { private set; get; }

        private Player[,] board;

        public Board(int width, int height)
        {
            if (width < 3 || height < 3 || width > 9999 || height > 9999)
            {
                throw new InvalidBoardSizeException();
            }

            this.Width = width;
            this.Height = height;

            this.board = new Player[width, height];
        }

        public Player GetPosition(Position position)
        {
            return this.board[position.X, position.Y];
        }

        public Player GetPosition(int x, int y)
        {
            return this.board[x, y];
        }

        public void SetMove(Player player, Position position)
        {
            if (player == Player.None)
            {
                throw new InvalidPlayerCodeException();
            }

            if (position.X > this.Width || position.Y > this.Height)
            {
                throw new InvalidMoveException(position);
            }

            if (this.GetPosition(position) != Player.None)
            {
                throw new InvalidMoveException(position);
            }


            board[position.X, position.Y] = player;
        }

    }

    public class InvalidBoardSizeException : Exception
    {
    }

    public class InvalidPlayerCodeException : Exception
    {
    }

    public class InvalidMoveException : Exception
    {
        public Position Position { get; private set; }

        public InvalidMoveException(Position position)
        {
            this.Position = position;
        }
    }

}
