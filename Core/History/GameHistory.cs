﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Core.History
{
    public class GameHistory
    {
        private LinkedList<Move> history;

        public GameHistory()
        {
            this.history = new LinkedList<Move>();
        }

        public void Add(Board.Player player, Position position)
        {
            this.history.AddLast(new Move(player, position));
        }

        public Move popLastMove()
        {
            Move last = this.history.Last.Value;
            this.history.RemoveLast();
            return last;
        }

        public IEnumerable<Move> GetEnumerable()
        {
            return this.history;
        }

        public void saveToFile(String path)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this.history);
            stream.Close();
        }

        public static GameHistory createFromFile(String path, bool ifNotExistCreateNew = false)
        {
            try {
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                LinkedList<Move> list = (LinkedList<Move>)formatter.Deserialize(stream);
                stream.Close();

                GameHistory gameHistory = new GameHistory();
                gameHistory.history = list;
                return gameHistory;
            }
            catch (System.IO.FileNotFoundException e)
            {
                return new GameHistory();
            }
        }
    }

    [Serializable]
    public class Move : ISerializable
    {
        public readonly Board.Player Player;
        public readonly Position Position;

        public Move(Board.Player player, Position position)
        {
            Player = player;
            Position = position;
        }

        public Move(SerializationInfo info, StreamingContext ctxt)
        {
            this.Player = (Board.Player) info.GetValue("player", typeof (Board.Player));
            this.Position = new Position(
                (int) info.GetValue("position_x", typeof(int)),
                (int) info.GetValue("position_y", typeof(int))
            );
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("position_x", this.Position.X);
            info.AddValue("position_y", this.Position.Y);
            info.AddValue("player", this.Player);
        }
    }
}
