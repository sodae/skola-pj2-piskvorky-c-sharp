﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.History
{
    public class Listener : IGameListener
    {
        private GameHistory gameHistory;

        public Listener(GameHistory gameHistory)
        {
            if (gameHistory == null) throw new NullReferenceException();
            this.gameHistory = gameHistory;
        }

        public void OnGameStarted(Game game)
        {
        }

        public void OnGameFinished(Game game, IPlayer winner)
        {
        }

        public void OnInvalidMove(Game game, IPlayer player, Position place)
        {
        }

        public void OnMove(Game game, IPlayer player, Position place)
        {
            this.gameHistory.Add(game.BoardView[place.X, place.Y], place);
        }
    }
}
