﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Core;
using Core.History;
using Core.Players;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            bool loadedGame = false;
            GameHistory gameHistory = new GameHistory();
            while(true) {
                try
                {
                    Board board = XmlBoardCreator.CreateBoard("board.xml", () => new Board(20, 20));
                    Game game = new Game(board);
                    //Game game = new Game(new Board(20, 20));
                    game.AddListener(new GameListener());
                    game.AddListener(new Core.History.Listener(gameHistory));

                    CommandController commandController = new CommandController(game, gameHistory);
                    game.SetPlayerO(new Player("Hráč", commandController));
                    game.SetPlayerX(new DummyPlayer("Two"));
                    IPlayer winner = loadedGame ? game.ContinueHistory(gameHistory) : game.Start();
                    System.Console.WriteLine(winner.ToString());
                    return;
                }
                catch (LoadGameExcetpion e)
                {
                    if (e.IsLoadSavedGame())
                    {
                        gameHistory = GameHistory.createFromFile(e.GetFileSave());
                        loadedGame = true;
                    }
                    else
                    {
                        gameHistory = new GameHistory();
                        loadedGame = false;
                    }
                }
                catch (InvalidBoardSizeException e)
                {
                    System.Console.WriteLine("Invalid size board, exiting...");
                    return;
                }
            }
        }
    }
}
