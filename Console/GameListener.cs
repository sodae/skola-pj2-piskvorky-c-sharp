﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;

namespace Console
{
    class GameListener:IGameListener
    {
        public void OnGameStarted(Game game)
        {
            System.Console.WriteLine("Game started");
        }

        public void OnGameFinished(Game game, IPlayer winner)
        {
            System.Console.WriteLine("Game finnished " + winner.ToString());
            System.Console.ReadKey();
        }

        public void OnInvalidMove(Game game, IPlayer player, Position place)
        {
            System.Console.WriteLine("Invalid move {0} on X {1}, Y {2}", player.ToString(), place.X, place.Y);
        }

        public void OnMove(Game game, IPlayer player, Position place)
        {
            System.Console.Clear();
            System.Console.WriteLine("Move {0} na X {1} a Y {2}", player.ToString(), place.X, place.Y);

            BoardView boardView = game.BoardView;

            for (int x = 0; x < boardView.Width; x++)
            {
                for (int y = 0; y < boardView.Height; y++)
                {
                    Board.Player onBoardPlayer = boardView[x, y];
                    var o = "";
                    if (onBoardPlayer == Board.Player.None) o = " ";
                    if (onBoardPlayer == Board.Player.X) o = "X";
                    if (onBoardPlayer == Board.Player.O) o = "O";
                    System.Console.Write(o);
                }
                System.Console.WriteLine();
            }
        }
    }
}
