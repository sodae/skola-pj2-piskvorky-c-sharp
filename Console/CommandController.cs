﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core;
using Core.History;

namespace Console
{
    public class CommandController
    {
        private GameHistory gameHistory;

        private Game game;

        public CommandController(Game game, GameHistory gameHistory)
        {
            this.game = game;
            this.gameHistory = gameHistory;
        }

        public void dispatch(String command)
        {
            var b = this.loadingGame(command)
                 || this.helpMenu(command);
        }

        private bool loadingGame(String command)
        {
            Match match = new Regex("^[\\s]*(?<method>load|save|restart)[\\s]*(?<filename>[a-z]+)?[\\s]*$").Match(command);
            if (match.Success)
            {
                string filename = (match.Groups["filename"].Value ?? "save") + ".bin";
                if (match.Groups["method"].Value == "save")
                {
                    this.gameHistory.saveToFile(filename);
                }
                else if (match.Groups["method"].Value == "restart")
                {
                    throw new LoadGameExcetpion();
                }
                else
                {
                    throw new LoadGameExcetpion(filename);
                }
                return true;
            }
            return false;
        }

        private bool helpMenu(String command)
        {
            Match match = new Regex("^[\\s]*help[\\s]*$").Match(command);
            if (!match.Success) return false;
            System.Console.WriteLine("save [filename] - save game");
            System.Console.WriteLine("load [filename] - load game");
            System.Console.WriteLine("restart - new game");
            return true;
        }
    }

    public class LoadGameExcetpion : InterruptionGameException
    {
        private String fileSave = null;

        public LoadGameExcetpion()
        {
        }

        public LoadGameExcetpion(string fileSave)
        {
            this.fileSave = fileSave;
        }

        public bool IsLoadSavedGame()
        {
            return this.fileSave != null;
        }

        public String GetFileSave()
        {
            return this.fileSave;
        }
    }

    public class InterruptionGameException : Exception
    {
    }
}
