﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core;

namespace Console
{
    class Player : IPlayer
    {
        private String name;

        private CommandController commandController;

        public Player(String name, CommandController commandController)
        {
            this.name = name;
            this.commandController = commandController;
        }

        public override string ToString()
        {
            return name;
        }

        public Position Move(BoardView boardView, Position lastMove)
        {
            for (;;)
            {
                System.Console.Write("Zadejte souradnice (X, Y): ");
                String input = System.Console.ReadLine();
                if (input == null) continue;
                Match match = new Regex("[\\s]*(?<X>[0-9]+)( |,|, )(?<Y>[0-9]+)[\\s]*$").Match(input);
                if (match.Success)
                {
                    return new Position(Int32.Parse(match.Groups["X"].Value), Int32.Parse(match.Groups["Y"].Value));
                }
                else
                {
                    commandController.dispatch(input);
                }
            }
        }
    }
}
